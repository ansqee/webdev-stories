from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story3home

class UnitTestStory3(TestCase):
    def test_story3_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story3_using_view_func(self):
        found = resolve('/story3/')
        self.assertEqual(found.func, story3home)

    def test_get_story3(self):
        response = Client().get(reverse("story3:story3home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story3home.html")

    def test_get_bonus(self):
        response = Client().get(reverse("story3:story3bonus"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story3bonus.html")
