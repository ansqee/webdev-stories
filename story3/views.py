from django.shortcuts import render

def story3home(request):
    return render(request, 'story3home.html')

def story3bonus(request):
    return render(request, 'story3bonus.html')

