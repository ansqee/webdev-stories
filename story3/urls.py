from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.story3home, name='story3home'),
    path('bonus/', views.story3bonus, name='story3bonus'),
]