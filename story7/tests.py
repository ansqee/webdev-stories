from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story7home

class UnitTestStory7(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story7_using_view_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7home)

    def test_get_story7(self):
        response = Client().get(reverse("story7:story7home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7home.html")
