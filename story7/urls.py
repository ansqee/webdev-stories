from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.story7home, name='story7home'),
]