$( function() {
    $("#sortable").sortable();
} );

$('.toggle').click(function(e) {
    e.preventDefault();

    let $this = $(this);

    if ($this.parent().next().hasClass('show')) {
        $this.parent().next().removeClass('show');
        $this.parent().next().slideUp(350);
    } else {
        $this.parent().parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().parent().find('li .inner').slideUp(350);
        $this.parent().next().toggleClass('show');
        $this.parent().next().slideToggle(350);
    }

});

$('.parent').click(function() {
    var $this = $(this).parent().next();
});

$('.down').click(function() {
    let $this = $(this).parent().parent().parent();
    $this.next().insertBefore($this);
});

$('.up').click(function() {
    let $this = $(this).parent().parent().parent();
    $this.prev().insertAfter($this);
});
