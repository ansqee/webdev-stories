from django.shortcuts import render
from django.http import JsonResponse, response
import urllib3
from json import loads

def story8home(request):
    context = {'page_title': 'API'}
    return render(request, 'story8home.html', context)

def search(request):
    http = urllib3.PoolManager()
    response = http.request(
        'GET', "https://www.googleapis.com/books/v1/volumes?" + request.GET.urlencode())
    context = loads(response.data.decode('utf-8'))
    return JsonResponse(context)
