var noimage = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/200px-No_image_3x4.svg.png";

$('#search-form').submit(e => {
    e.preventDefault();
    let search = $("#search-url").val()
    getApi(search)
})

function getApi(search) {
    $.ajax({
        url: window.location.origin + "/story8/api",
        contentType: "application/json",
        data: {
        "q": search,
        "maxResults": 20,
        },
        dataType: 'json',
        success: changeInner
    });
}

function changeInner(result) {
    let parent = $("#books");
    let inner = "";
    let json = result.items;
    for (let i = 0; i < json.length; i++) {
        let data = json[i].volumeInfo
        inner += '<div class="card">'
        inner += '<a target="_blank" href="' + data.infoLink + '">'
        inner += '<div class="img-div"><img class="img-card" src="' + (data.imageLinks === undefined ? noimage : data.imageLinks.thumbnail) + '"/></div>'
        inner += "<h5>" + truncateString(data.title, 70) + "</h5>"
        inner += "<p>" + truncateString(data.authors, 20) + "</p>"
        inner += "</a></div>"
    }
        parent.html(inner)
}

function truncateString(str, length) {
    return str === undefined ? "-" : str.length > length ? str.substring(0, length - 3) + '...' : str
}
