from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8home, name='story8home'),
    path('api', views.search, name="api")
]