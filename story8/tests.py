from django.test import TestCase, Client
from django.urls import reverse
from json import loads

class UnitTestStory8(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_story8home_views(self):
        response = Client().get(reverse('story8:story8home'), follow=True)
        self.assertIn("Search Google Books", response.content.decode('utf-8'))
    
    def test_api_views(self):
        response = Client().get(reverse('story8:api')+"?q=test", follow=True)
        self.assertIn("items", response.content.decode('utf-8'))