from django.urls import path
from . import views

app_name = 'story1'

urlpatterns = [
    path('', views.story1home, name='story1home'),
]