from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story1home

class UnitTestStory1(TestCase):
    def test_story1_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story1_using_view_func(self):
        found = resolve('/story1/')
        self.assertEqual(found.func, story1home)

    def test_get_story1(self):
        response = Client().get(reverse("story1:story1home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story1home.html")
