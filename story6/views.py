from django.shortcuts import render, redirect
from .models import Kegiatan, Pendaftar
from .forms import FormKegiatan, FormPendaftar

def story6home(request):
    items = Kegiatan.objects.all()
    pendaftar = Pendaftar.objects.all()
    context = {'satu_kegiatan' : items}
    form = FormPendaftar()
    if request.method == 'POST':
        kegiatan = Kegiatan.objects.get(id = request.POST['id'])
        peserta = Pendaftar.objects.create(nama_pendaftar=request.POST['nama_pendaftar'], ikut_kegiatan=kegiatan)
        return redirect("story6:story6home")
    context = {"kegiatan" : items, "form_peserta" : form, "peserta": pendaftar}
    return render(request, 'story6home.html', context)

def story6form(request):
    if request.method == 'POST':
        form = FormKegiatan(request.POST)
        if form.is_valid():
            form.save()
        return redirect('story6:story6home')
    else:
        form = FormKegiatan()
        return render(request, 'story6form.html', {'FormKegiatan':form})

