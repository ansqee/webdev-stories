from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.story6home, name ='story6home'),
    path('form/', views.story6form, name = 'story6form'),
]