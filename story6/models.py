from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 30)

    def __str__(self):
        return self.nama_kegiatan

class Pendaftar(models.Model):
    nama_pendaftar = models.CharField(max_length = 30)
    ikut_kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_pendaftar
