from django.contrib import admin
from .models import Kegiatan, Pendaftar

class AuthorAdmin(admin.ModelAdmin):
    pass
admin.site.register(Kegiatan, AuthorAdmin)
admin.site.register({Pendaftar}, AuthorAdmin)