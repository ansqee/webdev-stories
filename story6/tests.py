from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story6home
from .models import Kegiatan, Pendaftar

class UnitTestKegiatan(TestCase):
    def test_kegiatan_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_using_view_func(self):
        found = resolve('/story6/') #ngecek yg dibuka tuh sama kek di views apa g functnya
        self.assertEqual(found.func, story6home)

    def test_kegiatan_dan_pendaftar(self):
        activity = Kegiatan.objects.create(nama_kegiatan='Menangis')
        orang = Pendaftar.objects.create(ikut_kegiatan=activity, nama_pendaftar='Valen')
        count_kegiatan = Kegiatan.objects.all().count()
        count_pendaftar = Pendaftar.objects.all().count()
        self.assertEqual(count_kegiatan, 1)
        self.assertEqual(activity.__str__(), 'Menangis')
        self.assertEqual(count_pendaftar, 1)
        self.assertEqual(orang.__str__(), 'Valen')

    def test_get_kegiatan(self):
        response = Client().get(reverse("story6:story6home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story6home.html")

    def test_form_kegiatan(self):
        response = Client().get(reverse("story6:story6form"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story6form.html")

    def test_post_kegiatan(self):
        response = Client().post(reverse("story6:story6form"), { 'nama_kegiatan' : 'Menangis' })
        get_kegiatan = Client().get(reverse('story6:story6home'))
        count_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_kegiatan, 1)
        self.assertContains(get_kegiatan, 'Menangis')
        response = Client().post(reverse("story6:story6home"), { 'nama_pendaftar' : 'Valen', 'id' : 1 })
        count_pendaftar = Pendaftar.objects.all().count()
        self.assertEqual(count_pendaftar, 1)
        self.assertEqual(response.status_code, 302)
        response = Client().post(reverse("story6:story6form"), {
            'nama_kegiatan' : 'halokakreysumpahkakppwsusahbgtkaknapadahwcapegatauanjerrrbtwtksusahbgtkasihaniwyawkkwkwwkwk'
        })
        count_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_kegiatan, 1)