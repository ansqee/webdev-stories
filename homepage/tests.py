from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index

class UnitTestHomePage(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_using_view_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_get_index(self):
        response = Client().get(reverse("homepage:index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")
