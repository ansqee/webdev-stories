from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story2home

class UnitTestStory2(TestCase):
    def test_story2_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story2_using_view_func(self):
        found = resolve('/story2/')
        self.assertEqual(found.func, story2home)

    def test_get_story2(self):
        response = Client().get(reverse("story2:story2home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story2home.html")
