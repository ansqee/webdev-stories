from django.urls import path
from . import views

app_name = 'story2'

urlpatterns = [
    path('', views.story2home, name='story2home'),
]