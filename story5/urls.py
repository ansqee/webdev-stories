from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.story5home, name='story5home'),
    path('form/', views.story5form, name='story5form'),
    path('<int:num>/', views.permatkul, name='permatkul') #path variabel dimasukin lewat sini trs dipake ke view wkwwkwkwk
]