from django.shortcuts import render, redirect
from .models import sebuah_matkul
from .forms import form_matkul

def story5home(request):
    items = sebuah_matkul.objects.all()
    context = {'matkul' : items}
    return render(request, 'story5home.html', context)

def story5form(request):
    if request.method == 'POST':
        form = form_matkul(request.POST)
        if form.is_valid():
            form.save()
        return redirect('story5:story5home')
    else:
        form = form_matkul()
        return render(request, 'story5form.html', {'form_matkul':form})

def permatkul(request, num):
    items = sebuah_matkul.objects.get(id=num)
    if request.method == 'POST':
        items.delete()
        return redirect('story5:story5home')
    context = {'matkul': items}
    return render(request, 'story5matkul.html', context)