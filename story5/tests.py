from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story5home, permatkul
from .models import sebuah_matkul

class UnitTestStory5(TestCase):
    def test_story5_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story5_using_view_func(self):
        found = resolve('/story5/') #ngecek yg dibuka tuh sama kek di views apa g functnya
        self.assertEqual(found.func, story5home)

    def test_get_story5home(self):
        response = Client().get(reverse("story5:story5home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story5home.html")

    def test_get_story5form(self):
        response = Client().get(reverse("story5:story5form"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story5form.html")

    def test_add_matkul(self):
        matkul = sebuah_matkul.objects.create(
            mata_kuliah = "PPW",
            dosen_pengajar = "Ka Rey",
            jumlah_sks = 3,
            deskripsi = "Matkul yang sangat menguras tenaga, duarrrr",
            semester_thn = "Gasal 2020/2021",
            ruang_kelas = "GMeet wkwk"
        )
        count_matkul = sebuah_matkul.objects.all().count()
        self.assertEqual(count_matkul, 1)
        self.assertEqual(matkul.__str__(), 'PPW')

    def test_post_matkul(self):
        response = Client().post(reverse("story5:story5form"), {
            'mata_kuliah' : "PPW",
            'dosen_pengajar' : "Ka Rey",
            'jumlah_sks' : 3,
            'deskripsi' : "Matkul yang sangat menguras tenaga, duarrrr",
            'semester_thn' : "Gasal 2020/2021",
            'ruang_kelas' : "GMeet wkwk"
        })
        matkul = Client().get(reverse('story5:story5home'))
        count_matkul = sebuah_matkul.objects.all().count()
        self.assertEqual(count_matkul, 1)
        self.assertEqual(response.status_code, 302)
        self.assertContains(matkul, 'PPW')

    def test_get_permatkul(self):
        response = Client().post(reverse("story5:story5form"), {
            'mata_kuliah' : "PPW",
            'dosen_pengajar' : "Ka Rey",
            'jumlah_sks' : 3,
            'deskripsi' : "Matkul yang sangat menguras tenaga, duarrrr",
            'semester_thn' : "Gasal 2020/2021",
            'ruang_kelas' : "GMeet wkwk"
        })
        count_matkul = sebuah_matkul.objects.all().count()
        self.assertEqual(count_matkul, 1)
        self.assertEqual(response.status_code, 302)

        response2 = Client().get(reverse("story5:permatkul", args=['1']))
        self.assertEqual(response2.status_code, 200)

        response3 = Client().post(reverse("story5:permatkul", args=['1']))
        self.assertEqual(response3.status_code, 302)



