from django import forms
from .models import sebuah_matkul

class form_matkul(forms.ModelForm):
    class Meta:
        model = sebuah_matkul # nama model
        fields = '__all__' # buat munculin field yg ada di model
        widgets = {
            'mata_kuliah': forms.TextInput(attrs={'class': 'fieldclass'}),
        }