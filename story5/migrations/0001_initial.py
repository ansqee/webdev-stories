# Generated by Django 3.1.2 on 2020-10-16 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='sebuah_matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mata_kuliah', models.CharField(max_length=50)),
                ('dosen_pengajar', models.CharField(max_length=25)),
                ('jumlah_sks', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)], default=1, max_length=1)),
                ('deskripsi', models.CharField(max_length=100)),
                ('semester_thn', models.CharField(choices=[('Gasal 2019/2020', 'Gasal 2019/2020'), ('Genap 2019/2020', 'Genap 2019/2020'), ('Gasal 2020/2021', 'Gasal 2020/2021'), ('Genap 2020/2021', 'Genap 2020/2021'), ('Gasal 2021/2022', 'Gasal 2021/2022'), ('Genap 2021/2022', 'Genap 2021/2022')], max_length=20)),
                ('ruang_kelas', models.CharField(max_length=25)),
            ],
        ),
    ]
