from django.db import models

class sebuah_matkul(models.Model):
    sks_choices = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)]
    semester_choices = [
        ("Gasal 2019/2020", "Gasal 2019/2020"),
        ("Genap 2019/2020", "Genap 2019/2020"),
        ("Gasal 2020/2021", "Gasal 2020/2021"),
        ("Genap 2020/2021", "Genap 2020/2021"),
        ("Gasal 2021/2022", "Gasal 2021/2022"),
        ("Genap 2021/2022", "Genap 2021/2022"),
    ]
        
    mata_kuliah = models.CharField(max_length = 50)
    dosen_pengajar = models.CharField(max_length = 25)
    jumlah_sks = models.IntegerField(choices=sks_choices, default=1)
    deskripsi = models.TextField(max_length = 200)
    semester_thn = models.CharField(choices=semester_choices, max_length = 20)
    ruang_kelas = models.CharField(max_length = 25)

    def __str__(self): # muncul di admin
        return self.mata_kuliah
